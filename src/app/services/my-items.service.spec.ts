import { TestBed } from '@angular/core/testing';

import { MyItemsService } from './my-items.service';

describe('MyItemsService', () => {
  let service: MyItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addItem function should add an item to the items array', () => {
    service.addItem(5);
    expect(service.items).toContain(5);
  });
});
