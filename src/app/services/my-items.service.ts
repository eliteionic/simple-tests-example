import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MyItemsService {
  public items: number[] = [];

  constructor() {}

  addItem(item: number) {
    this.items.push(item);
  }
}
