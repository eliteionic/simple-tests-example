it('loads examples', () => {
  cy.visit('/');
  cy.get('#container strong').should('have.text', 'Ready to create an app?');
});
